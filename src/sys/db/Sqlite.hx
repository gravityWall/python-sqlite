

package sys.db;

import python.Tuple;
import haxe.io.Bytes;

import python.Syntax;

import sys.db.Connection;
import sys.db.ResultSet;

using StringTools;


@:pythonImport("sqlite3")
private extern class PythonSqlite {

    public static function connect(dbPath:String):PythonSqliteConnection;

}

@:pythonImport("sqlite3")
private extern class PythonSqliteConnection {

    public function commit():Void;

    public function close():Void;

    public function rollback():Void;
    
    public function cursor():Cursor;

    public var row_factory:(cursor:Cursor, row:Row)->Dynamic;

    public static function Row(cursor:Cursor, row:Row):Dynamic;

}

private extern class Cursor {

    public function execute(query:String):Cursor;

    public function fetchone():Row;
    
    public function fetchall():Array<Row>;

    public var rowcount(default, never):Int;

    public var lastrowid(default, never):Int;

    public var description(default,never):Null<Array<Tuple<String>>>;


}

typedef Row = python.Dict<String,Dynamic>; //Not quite the same but works well enough

@:coreApi class Sqlite {
    public static function open(file:String):Connection {
        var connection = new SqliteConnection(file);
        return connection;
    }
}


private class SqliteConnection implements Connection {
    
    private var connection:PythonSqliteConnection;
    private var cursor:Cursor;
    private var filename:String;
    
    public function new(filename:String) {
        this.filename = filename;
        connection = PythonSqlite.connect(filename);
        connection.row_factory = Syntax.code('sys_db__Sqlite_PythonSqlite.Row');
        cursor = connection.cursor();
    }

    public function close() {
        connection.close();
    }

    public function dbName():String {
        return "SQLite";
    }

    public function lastInsertId():Int {
        return cursor.lastrowid;
    }

    public function request(sql:String):ResultSet {

        try {

            cursor = connection.cursor();
            cursor.execute(sql);
            return new ResultSetImpl(cursor);
        
        } catch (e:String) {

            throw "Error while executing " + sql + " (" + e + ")";

        }

    }

    public function escape(s:String) {

        return s.split("'").join("''");

    }

    public function quote(s:String):String {

		if (s.indexOf("\000") >= 0) {
			var hexChars = new Array<String>();
			for (i in 0...s.length)
				hexChars.push(StringTools.hex(StringTools.fastCodeAt(s, i), 2));
			return "x'" + hexChars.join("") + "'";
		}
		return "'" + s.split("'").join("''") + "'";
        
    }

    public function rollback():Void {

        request("ROLLBACK");

    }

    public function startTransaction():Void {

        request("BEGIN TRANSACTION");

    }

    public function commit():Void {

        request("COMMIT");

    }

	public function addValue(s:StringBuf, v:Dynamic):Void {
		switch (Type.typeof(v)) {
			case TNull, TInt:
				s.add(v);
			case TBool:
				s.add(v ? 1 : 0);
			case TClass(haxe.io.Bytes):

				s.add(quote((v : haxe.io.Bytes).toString()));

                
			case _:
				s.add(quote(Std.string(v)));
		}
	}


}

private class ResultSetImpl implements ResultSet {

    private var cursor:Cursor;

    private var data:List<Dynamic>;

    public var length(get, null):Int;

    public var nfields(get, null):Int;

    private var columns:Array<String>;

    public var lastInsertRowid(get, null):Int;

    private var dataSource:Array<Row>;

    public function new(cursor:Cursor) {

        this.cursor = cursor;
        dataSource = cursor.fetchall();
        data = new List();
        columns = [];
        if (cursor.description != null) {
            var descriptionTuples = cursor.description;
            for (tuple in descriptionTuples) {
                columns.push(tuple[0]);
            }
        }
        while (dataSource.length > 0) {
            /* the pop() behavior on native Python lists is different, and they seem to
            come in reverse order. That's why there's this syntax weirdness below.*/
            var row:Row = Syntax.code('{0}.pop({1})', dataSource, (dataSource.length - 1));
            var keys = row.keys();
            columns = cast keys;
            var obj:Dynamic = {};
            for (key in keys) {
                Reflect.setField(obj, key, Syntax.code('{0}[{1}]', row, key));
                //Syntax.code('{0}.{1} = {2}', obj, key, row.get(columns.indexOf(key)));
            }
            data.push(obj);
            
        }
        
    }

    public function results():List<Dynamic> {

        return data;

    }

    private function get_nfields():Int {

        return columns.length;

    }

    public function getFieldsNames():Null<Array<String>> {

        return (columns.length != 0) ? columns : null;

    }

    public function next():Dynamic {

        return data.pop();

    }

    public function hasNext():Bool {

        return (data.length > 0);

    }

    public function get_length():Int {

        return data.length;

    }

    private function get_lastInsertRowid():Int {

        return cursor.lastrowid;

    }

    public function getFloatResult(columnPosition:Int):Float {

        return Std.parseFloat(getResult(columnPosition));

    }

    public function getIntResult(columnPosition:Int):Int {

        return Std.parseInt(getResult(columnPosition));

    }

    public function getResult(columnPosition:Int):String {

        var obj = data.first();
        var key = columns[columnPosition];
        return Std.string(Reflect.field(obj, key));

    }

}
