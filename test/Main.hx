class Main {
	static function main() {
		var runner = new utest.Runner();
		utest.ui.Report.create(runner);
        runner.addCase(new RecordMacrosSqliteTest("test.db"));
        runner.addCase(new TestSqliteConnection());
        runner.addCase(new TestSqliteResultSet());

		runner.run();
	}
}

