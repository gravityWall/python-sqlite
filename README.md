# python-sqlite

This Haxe library enables SQLite support for the Python target. It is compatible with record-macros, and passes almost all of its tests.

Just add `-lib python-sqlite` to your HXML file and then you can use it with Python. It uses the native `sqlite3` module for Python.

While this library seems to be working well, it may not be production-ready; make sure it works with your test cases.

Credits to the Haxe Foundation for the SQLite libraries for other targets. Many functions were straight copy pasted from those files, and they also wrote the `record-macros` test suit. Many thanks to them.

## Caveats

* Due to a Haxe bug, using this library and/or `record-macros` will cause the generated Python code to not initialize some static fields in the correct order, causing a runtime error. As of 29/04/2023, this issue affects Haxe 4.3.1 and is being tracked [here](https://github.com/HaxeFoundation/haxe/issues/10562) and [here](https://github.com/HaxeFoundation/haxe/issues/10902). Meanwhile, you can add the following line to your `*.hxml` file as a workaround. If you're using Windows, you need `sed` to be available for it to work.

```
--cmd sed -i '/_hx_class_name = "python.Boot"/c\    _hx_class_name = "python.Boot"\n    keywords = set(["and", "del", "from", "not", "with", "as", "elif", "global", "or", "yield", "assert", "else", "if", "pass", "None", "break", "except", "import", "raise", "True", "class", "exec", "in", "return", "False", "continue", "finally", "is", "try", "def", "for", "lambda", "while"])\n    prefixLength = len("_hx_")' main.py
``` 

* The haxelib version of `record-macros` is out of date and doesn't compile with recent Haxe versions. There is [a repository](https://github.com/filt3rek/record-macros) with fixes available, and you can use instead for now. This affects all targets. You can enable it with the following command:

```
haxelib git record-macros https://github.com/filt3rek/record-macros
```